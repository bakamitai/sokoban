#define STRING_IMPLEMENTATION
#include "string.h"

#define TEST(boolean) do {                                              \
        if(!(boolean)) {                                                \
            printf("Test failed at %s:%d\n", __FILE__, __LINE__);       \
            number_of_tests++;                                          \
        } else {                                                        \
            number_of_tests++;                                          \
            number_of_tests_passed++;                                   \
        }                                                               \
    } while(0)

int main()
{
    int number_of_tests = 0, number_of_tests_passed = 0;

    // Last token shorter than delimiter
    String iterate_me = wrap_cstring("123baba456baba789");
    String token = {0};
    StringIterator iter = create_string_iterator(iterate_me, wrap_cstring("baba"));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("123")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("456")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("789")));

    TEST(!next(&iter, &token));
    TEST(equal(token, wrap_cstring("789")));

    // Last token longer than delimiter
    iterate_me = wrap_cstring("123baba456baba78910111213");
    iter = create_string_iterator(iterate_me, wrap_cstring("baba"));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("123")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("456")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("78910111213")));

    TEST(!next(&iter, &token));
    TEST(equal(token, wrap_cstring("78910111213")));


    // Last token equal in length to delimiter
    iterate_me = wrap_cstring("123baba456baba1213");
    iter = create_string_iterator(iterate_me, wrap_cstring("baba"));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("123")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("456")));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("1213")));

    TEST(!next(&iter, &token));
    TEST(equal(token, wrap_cstring("1213")));


    // All delimiters all the time
    iterate_me = wrap_cstring("babababababababababa"); // 5 tokens
    iter = create_string_iterator(iterate_me, wrap_cstring("baba"));

    // 1
    TEST(next(&iter, &token));
    TEST(token.length == 0);

    // 2
    TEST(next(&iter, &token));
    TEST(token.length == 0);

    // 3
    TEST(next(&iter, &token));
    TEST(token.length == 0);

    // 4
    TEST(next(&iter, &token));
    TEST(token.length == 0);

    // 5
    TEST(next(&iter, &token));
    TEST(token.length == 0);

    // next returns false after getting to the end of the string
    TEST(!next(&iter, &token));
    TEST(token.length == 0);

    // Delimiter is longer than entire string
    iterate_me = wrap_cstring("Hello Sailor!");
    iter = create_string_iterator(iterate_me, wrap_cstring("My Brain Has Been Replaced By A Radio"));

    TEST(next(&iter, &token));
    TEST(equal(token, wrap_cstring("Hello Sailor!")));

    TEST(!next(&iter, &token));
    TEST(equal(token, wrap_cstring("Hello Sailor!")));

    printf("%d/%d tests passed!\n", number_of_tests_passed, number_of_tests);
    return 0;
}
