#ifndef STRING
#define STRING
#include <stddef.h>

typedef struct
{
    char *data;
    size_t length;
    size_t capacity;
} String;

typedef struct
{
    String base;
    String delimiter;
    int cursor;
} StringIterator;

typedef struct
{
    String base;
    int cursor;
} LinesIterator;

StringIterator create_string_iterator(String base, String delimiter);
LinesIterator create_lines_iterator(String base);
int next(StringIterator *iter, String *output);
/* int number_of_tokens(StringIterator iter); */
int read_to_string(char *filename, String *output);
String string_new(unsigned long capacity);
String copy_string(String s);
String wrap_cstring(char *str);
int parse_int(String s);
String strip(String s);
int find(String s, char c);
String split_left(String s, size_t index);
String split_right(String s, size_t index);
String substring(String s, size_t start, size_t end);
int common_chars(String a, String b);
int equal(String a, String b);
int fuzzy_match(String a, String b);
void print_string(String s);
int string_contains(String s, char c);
void string_push(String *s, char c);

#endif

#ifdef STRING_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>

void _maybe_realloc(String *s)
{
    while(s->length >= s->capacity)
    {
        s->data = realloc(s->data, s->capacity * 2 * sizeof(char));
        s->capacity *= 2;
    }
}

StringIterator create_string_iterator(String base, String delimiter)
{
    StringIterator iter = {
        .cursor = 0,
        .base = base,
        .delimiter = delimiter
    };

    return iter;
}

LinesIterator create_lines_iterator(String base)
{
    LinesIterator iter = {
        .base = base,
        .cursor = 0,
    };

    return iter;
}

int next(StringIterator *iter, String *output)
{
    if(iter->cursor >= iter->base.length) return 0;

    output->data = iter->base.data + iter->cursor;
    output->length = 0;
    String delimiter = iter->delimiter;


    while(iter->cursor + delimiter.length <= iter->base.length)
    {
        if(strncmp(iter->base.data + iter->cursor, delimiter.data, delimiter.length) == 0)
        {
            iter->cursor += iter->delimiter.length;
            return 1;
        }
        iter->cursor++;
        output->length++;
    }

    // If remainder of the string is smaller than delimiter return the rest of the string.
    if(iter->base.length - iter->cursor < delimiter.length)
    {
        output->length += iter->base.length - iter->cursor;
        iter->cursor = iter->base.length;
    }

    return 1;
}

int next_line(LinesIterator *iter, String *output)
{
    if(iter->cursor >= iter->base.length) return 0;

    output->data = iter->base.data + iter->cursor;
    output->length = 0;


    while(iter->cursor < iter->base.length)
    {
        if(iter->base.data[iter->cursor] == '\r' && iter->cursor + 1 < iter->base.length && iter->base.data[iter->cursor + 1] == '\n')
        {
            iter->cursor += 2;
            break;
        }
        else if(iter->base.data[iter->cursor] == '\n')
        {
            iter->cursor++;
            break;
        }

        iter->cursor++;
        output->length++;
    }

    return 1;
}

/* int number_of_tokens(StringIterator iter) */
/* { */
/*     int n; */
/*     for(n = 0; iter.cursor < iter.base.length; ++n) */
/*     { */
/*         while(iter.cursor < iter.base.length && iter.base.data[iter.cursor++] != iter.delimiter); */
/*     } */
/*     return n; */
/* } */

int read_to_string(char *filename, String *output)
{
    struct stat statbuf = {0};

    if(stat(filename, &statbuf) == 0)
    {
        FILE *f;
        if((f = fopen(filename, "r")))
        {
            output->data = (char *) malloc(statbuf.st_size);
            output->length = output->capacity = statbuf.st_size;
            fread(output->data, 1, statbuf.st_size, f);
            fclose(f);
        }
        else
        {
            /* fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno)); */
            return 0;
        }
    }
    else
    {
        /* fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno)); */
        return 0;
    }

    return 1;
}


String string_new(unsigned long capacity)
{
    String new;
    new.data = (char *) malloc(capacity * sizeof(char));
    new.capacity = capacity;
    new.length = 0;
    return new;
}

String copy_string(String s)
{
    String new;
    new.data = (char *) malloc(s.length);
    new.length = s.length;
    new.capacity = s.length;
    memcpy(new.data, s.data, s.length);
    return new;
}

String wrap_cstring(char *str)
{
    String s = {0};
    s.data = str;
    while(*str++ != '\0') s.length++;
    s.capacity = s.length;
    return s;
}

int parse_int(String s)
{
    int order = 1;
    int result = 0;
    for(int i = s.length - 1; i >= 0; i--)
    {
        result += (s.data[i] - '0') * order;
        order *= 10;
    }
    return result;
}

String strip(String s)
{
    char c;
    
    while((c = s.data[s.length - 1]) == ' ' || c == '\t' || c == '\n' || c == '\r') s.length--;
    while((c = s.data[0]) == ' ' || c == '\t' || c == '\n' || c == '\r')
    {
        s.data++;
        s.length--;
    }

    return s;
}

int find(String s, char c)
{
    for(int i = 0; i < s.length; i++)
    {
        if(s.data[i] == c) return i;
    }

    return -1;
}

String split_left(String s, size_t index)
{
    assert(index < s.length);

    String left;
    left.data = s.data;
    left.length = index + 1;
    left.capacity = left.length;

    return left;
}

String split_right(String s, size_t index)
{
    assert(index < s.length);
    
    String right;
    right.data = s.data + index;
    right.length = s.length - index;
    right.capacity = right.length;

    return right;
}

String substring(String s, size_t start, size_t end)
{
    String sub;
    sub.data = s.data + start;
    sub.length = end - start;
    sub.capacity = sub.length;
    return sub;
}

int common_chars(String a, String b)
{
    int common = 0;
    for(int i = 0; i < a.length; i++)
    {
        for(int j = 0; j < b.length; j++)
        {
            if(a.data[i] == b.data[j])
            {
                common++;
                break;
            }
        }
    }
    return common;
}

int equal(String a, String b)
{
    if(a.length != b.length) return 0;

    for(int i = 0; i < a.length; i++)
        if(a.data[i] != b.data[i]) return 0;
    return 1;
}

char lowercase(char c)
{
    return c + (c >= 'A' && c <= 'Z') * 32;
}

int fuzzy_match(String a, String b)
{
    int b_pos = 0, match;
    char c1, c2;
    
    for(int i = 0; i < a.length; ++i)
    {
        match = 0;

        c1 = lowercase(a.data[i]);
        while(b_pos < b.length)
        {
            c2 = lowercase(b.data[b_pos++]);
            if(c1 == c2)
            {
                match = 1;
                break;
            }
        }
    }
    return match;
}

// This function doesn't work
void print_string(String s)
{
    for(int i = 0; i < s.length; i++)
    {
        putchar(s.data[i]);
    }
}

int string_contains(String s, char c)
{
    for(int i = 0; i < s.length; i++)
    {
        if(s.data[i] == c) return 1;
    }
    return 0;
}

void string_push(String *s, char c)
{
    _maybe_realloc(s);
    s->data[s->length++] = c;
}

#endif
