#!/bin/sh

[ -d ../build ] || mkdir ../build
cc -Wall -g -lX11 -iquote ../include sokoban.c -o ../build/sokoban
