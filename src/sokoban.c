/*
  TODO :P
  1. refactor parse_boards silly little function :P
  2. add error handling to parse_boards lol :P
*/

#define STRING_IMPLEMENTATION
#include "string.h"
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <inttypes.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef double f64;
typedef float f32;


#define panic(format, ...) _panic(__FILE__, __LINE__, (format) __VA_OPT__(,) __VA_ARGS__)
void _panic(const char *file, int line, const char *format, ...)
{
    fprintf(stderr, "Program panicked at: %s:%d\n", file, line);
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    exit(1);
}

typedef struct
{
    uint8_t *memory;
    size_t stack_pointer;
    size_t capacity;
} Arena;

void *allocate(Arena *arena, size_t num_bytes)
{
    if(arena->stack_pointer + num_bytes >= arena->capacity)
        panic("%lu is not enough memory lolz!\n", arena->capacity);

    void *return_pointer = arena->memory + arena->stack_pointer;
    arena->stack_pointer += num_bytes;
    return return_pointer;
}

void set_stack_pointer(Arena *arena, size_t sp)
{
    arena->stack_pointer = sp;
}

Arena new_arena(size_t capacity)
{
    Arena arena = {
        .memory = malloc(capacity),
        .stack_pointer = 0,
        .capacity = capacity,
    };
    return arena;
}

Arena levels;

/*
  Sokoban format.
  '#' = WALL  -- GREY
  ' ' = FLOOR -- WHITE
  '@' = START -- BLUE
  '.' = GOAL  -- RED
  '$' = BLOCK -- GREEN
  '*' = GOAL and BLOCK -- YELLOW
  '+' = START and GOAL -- PURPLE
 */
typedef enum
{
    FLOOR   = 1 << 0,
    WALL    = 1 << 1,
    GOAL    = 1 << 2,
    BLOCK   = 1 << 3,
    START   = 1 << 4,
    OUTSIDE = 1 << 5,
} CellType;

#define is_floor(cell) (((cell) >> 0) & 1)
#define is_wall(cell) (((cell) >> 1) & 1)
#define is_goal(cell) (((cell) >> 2) & 1)
#define is_block(cell) (((cell) >> 3) & 1)
#define is_start(cell) (((cell) >> 4) & 1)
#define is_outside(cell) (((cell) >> 5) & 1)

#define LEFT   1
#define RIGHT  2
#define UP     3
#define DOWN   4
#define PLAYER (1 << 4)

// 0x7 mask the first 3 bits
#define is_left(m) (((m) & 0x7) == LEFT)
#define is_right(m) (((m) & 0x7) == RIGHT)
#define is_up(m) (((m) & 0x7) == UP)
#define is_down(m) (((m) & 0x7) == DOWN)
#define is_player(m) (((m) >> 4) & 1)

#define MOVES_CAPACITY 256

static uint8_t moves[MOVES_CAPACITY];
static uint8_t moves_length;

static int window_width;
static int window_height;
#define MIN_WINDOW_WIDTH 100
#define MIN_WINDOW_HEIGHT 100

XImage *block_img;
XImage *wall_img;
XImage *floor_img;
XImage *guy_img;
XImage *goal_img;

#define abs(n) ((n) < 0 ? -(n) : (n))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define round_to_int(n) ((int) ((n) + 0.5))

typedef struct
{
    Display *display;
    Window window;
    GC gc;
    XImage *window_buffer;
    int window_width;
    int window_height;
    int x_offset;
    int y_offset;
    int cell_width;
    int cell_height;
    double x_scale;
    double y_scale;
} State;


typedef struct
{
    String title;
    size_t width;
    size_t height;
    char *data;
} Board;

typedef struct
{
    size_t length;
    size_t capacity;
    Board *data;
} BoardArray;

void draw_player(State state, int x, int y);
void draw_level(State state, Board b);

BoardArray make_board_array(size_t capacity)
{
    BoardArray b;
    b.length = 0;
    b.capacity = capacity;
    b.data = malloc(capacity * sizeof(Board));

    return b;
}

void push_board(BoardArray *arr, Board board)
{
    if(arr->length >= arr->capacity)
    {
        arr->capacity *= 2;
        arr->data = realloc(arr->data, arr->capacity * sizeof(Board));
    }

    arr->data[arr->length++] = board;
}

size_t longest_line(String board)
{
    StringIterator lines = create_string_iterator(board, wrap_cstring("\n"));
    String line = {0};

    size_t max = 0;
    while(next(&lines, &line))
    {
        max = line.length > max ? line.length : max;
    }

    return max;
}

size_t number_of_lines(String s)
{
    size_t lines = 0;
    for(int i = 0; i < s.length; i++) lines += s.data[i] == '\n';

    return lines;
}

void find_floors(Board b)
{
    int floor_stack[64];
    int floor_stack_length = 0;

    for(int i = 0; i < b.width * b.height; i++)
    {
        u8 cell = b.data[i];
        if(is_goal(cell))
        {
            b.data[i] |= FLOOR;
            floor_stack[floor_stack_length++] = i;   
        }
    }

    while(floor_stack_length)
    {
        int current_index = floor_stack[--floor_stack_length];
        u8 above = b.data[current_index - b.width];
        u8 below = b.data[current_index + b.width];
        u8 right = b.data[current_index + 1];
        u8 left = b.data[current_index - 1];

        if(!is_wall(above) && !is_floor(above))
        {
            b.data[current_index - b.width] |= FLOOR;
            floor_stack[floor_stack_length++] = current_index - b.width;
        }
        if(!is_wall(below) && !is_floor(below))
        {
            b.data[current_index + b.width] |= FLOOR;
            floor_stack[floor_stack_length++] = current_index + b.width;
        }
        if(!is_wall(right) && !is_floor(right))
        {
            b.data[current_index + 1] |= FLOOR;
            floor_stack[floor_stack_length++] = current_index + 1;
        }
        if(!is_wall(left) && !is_floor(left))
        {
            b.data[current_index - 1] |= FLOOR;
            floor_stack[floor_stack_length++] = current_index - 1;
        }
    }
}

Board parse_board(String board_string, String title)
{
    
    Board b;
    b.title = title;
    b.width = longest_line(board_string);
    b.height = number_of_lines(board_string);
    b.data = allocate(&levels, b.width * b.height * sizeof(char));


    /* int wall_seen = 0; */
    size_t board_index = 0;
    for(int i = 0; i < board_string.length; i++)
    {
        switch(board_string.data[i])
        {
            case '#':
            {
                /* wall_seen = 1; */
                b.data[board_index++] = WALL;
            } break;
            case ' ': b.data[board_index++] = OUTSIDE; break;
            case '@': b.data[board_index++] = START; break;
            case '.': b.data[board_index++] = GOAL; break;
            case '$': b.data[board_index++] = BLOCK; break;
            case '*': b.data[board_index++] = GOAL | BLOCK; break;
            case '+': b.data[board_index++] = START | GOAL; break;
            case '\n':
            {
                /* wall_seen = 0; */
                if((board_index % b.width) != 0)
                {
                    int extra = b.width - (board_index % b.width);
                    while(extra--)
                        b.data[board_index++] = OUTSIDE;
                }
            } break;
        }
    }
    find_floors(b);
    return b;
}

BoardArray parse_boards(char *board_file)
{
    String file = {0}, board = string_new(512), line = {0}, title = {0};
    LinesIterator lines;
    BoardArray boards = make_board_array(128);
    if(read_to_string(board_file, &file))
    {
        lines = create_lines_iterator(file);

        int count = 0;
        while(next_line(&lines, &line))
        {
            if(line.length == 0) continue;
            if(string_contains(line, ';'))
            {
                if(count++ > 0) push_board(&boards, parse_board(board, title));

                board.length = 0;
                title.data = line.data + 2;
                title.length = line.length - 2;
                continue;
            }

            for(int i = 0; i < line.length; i++)
            {
                string_push(&board, line.data[i]);
            }
            string_push(&board, '\n');
        }
        
        // Add final board to boards array because there is no ';' after the last level
        push_board(&boards, parse_board(board, title));

        // This will free all the title data so remember to remove this LOLMAO!!!!
        free(file.data);
        free(board.data);
    }
    else
    {
        fprintf(stderr, "Unable to open file %s\n%s\n", board_file, strerror(errno));
        exit(1);
    }
    return boards;
}

u32 blend_color(u32 dest, u32 src, u8 alpha)
{
    u32 dest_blue = dest & 0xFF;
    u32 dest_green = (dest >> 8) & 0xFF;
    u32 dest_red = (dest >> 16) & 0xFF;
     
    u32 src_blue = src & 0xFF;
    u32 src_green = (src >> 8) & 0xFF;
    u32 src_red = (src >> 16) & 0xFF;

    u32 blend_blue = ((src_blue * alpha) + (dest_blue * (255 - alpha))) >> 8;
    u32 blend_green = ((src_green * alpha) + (dest_green * (255 - alpha))) >> 8;
    u32 blend_red = ((src_red * alpha) + (dest_red * (255 - alpha))) >> 8;

    return (blend_red << 16) | (blend_green << 8) | blend_blue;
}

void draw_image(XImage *destination, XImage *source, int destination_x, int destination_y, int source_x, int source_y, int width, int height, double x_scale, double y_scale)
{
    if(destination_x >= destination->width || destination_y >= destination->height) return;
    if(destination_x < -width || destination_y < -height) return;

    if(destination_x + width >= destination->width) width = destination->width - destination_x;
    if(destination_y + height >= destination->height) height = destination->height - destination_y;
    if(destination_x < 0)
    {
        width += destination_x;
        destination_x = 0;
    }
    if(destination_y < 0)
    {
        height += destination_y;
        destination_y = 0;
    }

    if(source_x < 0 || source_x >= source->width) panic("source_x is outside of the image!\n");
    if(source_y < 0 || source_y >= source->height) panic("source_y is outside of the image!\n");
    /* if(source->width < width) panic("width larger than source->width\n"); */
    /* if(source->height < height) panic("height larger than source->height\n"); */

    u32 *destination_data = (u32 *) destination->data;
    u32 *source_data = (u32 *) source->data;
    for(int y = 0; y < height; y++)
    {
        for(int x = 0; x < width; x++)
        {
            int src_x = (int) ((double) x / x_scale);
            int src_y = (int) ((double) y / y_scale);
            u32 destination_color = destination_data[(y + destination_y) * destination->width + x + destination_x];
            u32 source_color = source_data[(src_y + source_y) * source->width + src_x + source_x];
            u8 alpha = source_color >> 24;
            destination_data[(y + destination_y) * destination->width + x + destination_x] = blend_color(destination_color, source_color, alpha);
        }
    }
}

void fill_rect(XImage *img, int start_x, int start_y, int width, int height, uint32_t color)
{
    if(start_x >= img->width || start_y >= img->height) return;
    if(start_x < -width || start_y < -height) return;

    if(start_x + width >= img->width) width = img->width - start_x;
    if(start_y + height >= img->height) height = img->height - start_y;
    if(start_x < 0)
    {
        width += start_x;
        start_x = 0;
    }
    if(start_y < 0)
    {
        height += start_y;
        start_y = 0;
    }

    uint32_t *image_data = (uint32_t *) img->data;
    uint32_t index = start_y * img->width;
    for(uint32_t y = start_y; y < start_y + height; y++)
    {
        for(uint32_t x = start_x; x < start_x + width; x++)
        {
            image_data[index + x] = color;
        }
        index += img->width;
    }
}

void play_animation(State state, Board b, int direction, int player_x, int player_y, int pushing_block, int block_x, int block_y)
{
    f64 number_of_frames = 8;

    f64 step_length = (f64) state.cell_width / number_of_frames;
    f64 render_start, render_time;
    struct timespec req = {0};

    // Defines a rectangle that contains all changes to the screen.
    int changed_x, changed_y, changed_width, changed_height;


    for(f64 position = 0; position < (f64) state.cell_width; position += step_length)
    {
        render_start = (f64) clock();
        changed_x = state.x_offset + player_x * state.cell_width;
        changed_y = state.y_offset + player_y * state.cell_height;

        // Draw floor in the cell the player occupies
        draw_image(state.window_buffer, floor_img,
                   state.x_offset + player_x * state.cell_width, state.y_offset + player_y * state.cell_height,
                   0, 0, state.cell_width, state.cell_height,
                   state.x_scale, state.y_scale);

        switch(direction)
        {
            case UP:
            {
                changed_width = state.cell_width;
                changed_height = state.cell_height * 2 + pushing_block * state.cell_height;
                changed_y -= (changed_height - state.cell_height);

                // Draw floor in cell player is moving into/block occupies
                draw_image(state.window_buffer, floor_img,
                           state.x_offset + player_x * state.cell_width, state.y_offset + (player_y - 1) * state.cell_height,
                           0, 0, state.cell_width, state.cell_height,
                           state.x_scale, state.y_scale);

                if(pushing_block)
                {
                    // Draw floor in cell block is moving into
                    draw_image(state.window_buffer, floor_img,
                               state.x_offset + block_x * state.cell_width, state.y_offset + (block_y - 1) * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

                // Draw player and block
                draw_player(state, player_x * state.cell_width, player_y * state.cell_height - round_to_int(position));
                if(pushing_block)
                {
                    draw_image(state.window_buffer, block_img,
                               state.x_offset + block_x * state.cell_width, state.y_offset + block_y * state.cell_height - round_to_int(position),
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

            } break;

            case DOWN:
            {
                changed_width = state.cell_width;
                changed_height = state.cell_height * 2 + pushing_block * state.cell_height;

                // Draw floor in cell player is moving into/block occupies
                draw_image(state.window_buffer, floor_img,
                           state.x_offset + player_x * state.cell_width, state.y_offset + (player_y + 1) * state.cell_height,
                           0, 0, state.cell_width, state.cell_height,
                           state.x_scale, state.y_scale);

                if(pushing_block)
                {
                    // Draw floor in cell block is moving into
                    draw_image(state.window_buffer, floor_img,
                               state.x_offset + block_x * state.cell_width, state.y_offset + (block_y + 1) * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

                // Draw player and block
                draw_player(state, player_x * state.cell_width, state.cell_height * player_y + round_to_int(position));
                if(pushing_block)
                {
                    draw_image(state.window_buffer, block_img,
                               state.x_offset + block_x * state.cell_width, state.y_offset + block_y * state.cell_height + round_to_int(position),
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

            } break;

            case LEFT:
            {
                changed_width = state.cell_width * 2 + pushing_block * state.cell_width;
                changed_height = state.cell_height;
                changed_x -= (changed_width - state.cell_width);

                // Draw floor in cell player is moving into/block occupies
                draw_image(state.window_buffer, floor_img,
                           state.x_offset + (player_x - 1) * state.cell_width, state.y_offset + player_y * state.cell_height,
                           0, 0, state.cell_width, state.cell_height,
                           state.x_scale, state.y_scale);

                if(pushing_block)
                {
                    // Draw floor in cell block is moving into
                    draw_image(state.window_buffer, floor_img,
                               state.x_offset + (block_x - 1) * state.cell_width, state.y_offset + block_y * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

                // Draw player and block
                draw_player(state, state.cell_width * player_x - round_to_int(position), player_y * state.cell_height);
                if(pushing_block)
                {
                    draw_image(state.window_buffer, block_img,
                               state.x_offset + block_x * state.cell_width - round_to_int(position), state.y_offset + block_y * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

            } break;

            case RIGHT:
            {
                changed_width = state.cell_width * 2 + pushing_block * state.cell_width;
                changed_height = state.cell_height;

                // Draw floor in cell player is moving into/block occupies
                draw_image(state.window_buffer, floor_img,
                           state.x_offset + (player_x + 1) * state.cell_width, state.y_offset + player_y * state.cell_height,
                           0, 0, state.cell_width, state.cell_height,
                           state.x_scale, state.y_scale);

                if(pushing_block)
                {
                    // Draw floor in cell block is moving into
                    draw_image(state.window_buffer, floor_img,
                               state.x_offset + (block_x + 1) * state.cell_width, state.y_offset + block_y * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

                // Draw player and block
                draw_player(state, state.cell_width * player_x + round_to_int(position), player_y * state.cell_height);
                if(pushing_block)
                {
                    draw_image(state.window_buffer, block_img,
                               state.x_offset + block_x * state.cell_width + round_to_int(position), state.y_offset + block_y * state.cell_height,
                               0, 0, state.cell_width, state.cell_height,
                               state.x_scale, state.y_scale);
                }

            } break;
        }

        // Draw changed part of window buffer to the screen
        XPutImage(state.display, state.window, state.gc, state.window_buffer, changed_x, changed_y, changed_x, changed_y, changed_width, changed_height);
        render_time = ((f64) clock() - render_start) / (f64) CLOCKS_PER_SEC;
        req.tv_nsec = 16666666l - (i64) (render_time * 1000000000 + 0.5); // 60 fps, might change this to be variable at some point
        nanosleep(&req, NULL);
    }
}

void move_right(State state, Board b, uint32_t *player_x, uint32_t *player_y)
{
    uint32_t x = *player_x, y = *player_y;
    uint8_t cell1 = b.data[x + 1 + y * b.width], cell2;
    if(!is_wall(cell1))
    {
        if(is_block(cell1))
        {
            cell2 = b.data[x + 2 + y * b.width];
            if(!is_wall(cell2) && !is_block(cell2))
            {
                b.data[x + 1 + y * b.width] &= ~BLOCK;
                play_animation(state, b, RIGHT, x, y, 1, x + 1, y);
                b.data[x + 2 + y * b.width] |= BLOCK;
                (*player_x)++;
                moves[moves_length++] = RIGHT;
            }
        }
        else
        {
            play_animation(state, b, RIGHT, x, y, 0, 0, 0);
            (*player_x)++;
            moves[moves_length++] = PLAYER | RIGHT;
        }
    }
}

void move_left(State state, Board b, uint32_t *player_x, uint32_t *player_y)
{
    uint32_t x = *player_x, y = *player_y;
    uint8_t cell1 = b.data[x - 1 + y * b.width], cell2;
    if(!is_wall(cell1))
    {
        if(is_block(cell1))
        {
            cell2 = b.data[x - 2 + y * b.width];
            if(!is_wall(cell2) && !is_block(cell2))
            {
                b.data[x - 1 + y * b.width] &= ~BLOCK;
                play_animation(state, b, LEFT, x, y, 1, x - 1, y);
                b.data[x - 2 + y * b.width] |= BLOCK;
                (*player_x)--;
                moves[moves_length++] = LEFT;
            }
        }
        else
        {
            play_animation(state, b, LEFT, x, y, 0, 0, 0);
            (*player_x)--;
            moves[moves_length++] = PLAYER | LEFT;
        }
    }
}

void move_down(State state, Board b, uint32_t *player_x, uint32_t *player_y)
{
    uint32_t x = *player_x, y = *player_y;
    uint8_t cell1 = b.data[x + (y + 1) * b.width], cell2;
    if(!is_wall(cell1))
    {
        if(is_block(cell1))
        {
            cell2 = b.data[x + (y + 2) * b.width];
            if(!is_wall(cell2) && !is_block(cell2))
            {
                b.data[x + (y + 1) * b.width] &= ~BLOCK;
                play_animation(state, b, DOWN, x, y, 1, x, y + 1);
                b.data[x + (y + 2) * b.width] |= BLOCK;
                (*player_y)++;
                moves[moves_length++] = DOWN;
            }
        }
        else
        {
            play_animation(state, b, DOWN, x, y, 0, 0, 0);
            (*player_y)++;
            moves[moves_length++] = PLAYER | DOWN;
        }
    }
}

void move_up(State state, Board b, uint32_t *player_x, uint32_t *player_y)
{
    uint32_t x = *player_x, y = *player_y;
    uint8_t cell1 = b.data[x + (y - 1) * b.width], cell2;
    if(!is_wall(cell1))
    {
        if(is_block(cell1))
        {
            cell2 = b.data[x + (y - 2) * b.width];
            if(!is_wall(cell2) && !is_block(cell2))
            {
                b.data[x + (y - 1) * b.width] &= ~BLOCK;
                play_animation(state, b, UP, x, y, 1, x, y - 1);
                b.data[x + (y - 2) * b.width] |= BLOCK;
                (*player_y)--;
                moves[moves_length++] = UP;
            }
        }
        else
        {
            play_animation(state, b, UP, x, y, 0, 0, 0);
            (*player_y)--;
            moves[moves_length++] = PLAYER | UP;
        }
    }
}

typedef enum
{
    BI_RGB = 0,
    BI_RLE8 = 1,
    BI_RLE4 = 2,
    BI_BITFIELDS = 3,
    BI_JPEG = 4,
    BI_PNG = 5,
    BI_ALPHABITFIELDS = 6,
    BI_CMYK = 11,
    BI_CMYKRLE8 = 12,
    BI_CMYKRLE4 = 13,
} CompressionMethod;

int load_bmp(char *filename, XImage *out_image)
{
    struct stat file_info;

    if(stat(filename, &file_info) == 0)
    {
        FILE *f = fopen(filename, "r");
        u64 buffer_length = file_info.st_size;
        u8 *buffer = (u8 *) malloc(buffer_length);

        u64 bytes_read = fread(buffer, 1, buffer_length, f);
        fclose(f);

        if(bytes_read == buffer_length)
        {
            i32 width, height, offset, compression;
            i16 color_depth;
            /* u32 red_mask, green_mask, blue_mask, alpha_mask, header_size; */

            /* two byte int containing color depth of bitmap is 28 bytes into the file */
            color_depth = *((i16 *) (buffer + 28));

            /* four byte int containing compression method used in the bitmap is 30 bytes into the file */
            compression = *((i32 *) (buffer + 30));

            /* four byte int containing the byte offset of the bitmap data is 10 bytes into the file */
            offset = *((i32 *) (buffer + 10));
            /* header_size = *((u32 *) (buffer + 14)); */
            /* alpha_mask = *((u32 *) (buffer + 10 + header_size - 16)); */
            /* red_mask = *((u32 *) (buffer + 10 + header_size - 12)); */
            /* green_mask = *((u32 *) (buffer + 10 + header_size - 8)); */
            /* blue_mask = *((u32 *) (buffer + 10 + header_size - 4)); */
            /* printf("header size = %u\nred mask:   0x%08x\ngreen mask: 0x%08x\nblue mask:  0x%08x\nalpha mask: 0x%08x\n", header_size, red_mask, blue_mask, green_mask, alpha_mask); */
            /* printf("Header size = %u\n", *((u32 *) (buffer + 14))); */

            /* width and height are 4 byte ints and are at byte offsets 18 and 22 */
            width = *((i32 *) (buffer + 18));
            height = *((i32 *) (buffer + 22));

            /* I deliberately don't free buffer here because the program is being closed and therefore the OS will clean up memory */
            if(buffer[0] != 'B' || buffer[1] != 'M') panic("Invalid file format in file %s\n", filename);   
            if(color_depth != 32) panic("Invalid color depth in bitmap %s, expected 24 bit color depth\n", filename);
            /* if(compression != BI_RGB) panic("Invalid compression method in bitmap %s\n", filename); */
            switch(compression)
            {
                case BI_RGB: printf("BI_RGB\n"); break;
                case BI_RLE8: printf("BI_RLE8\n"); break;
                case BI_RLE4: printf("BI_RLE4\n"); break;
                case BI_BITFIELDS: printf("BI_BITFIELDS\n"); break;
                case BI_JPEG: printf("BI_JPEG\n"); break;
                case BI_PNG: printf("BI_PNG\n"); break;
                case BI_ALPHABITFIELDS: printf("BI_ALPHABITFIELDS\n"); break;
                case BI_CMYK: printf("BI_CMYK\n"); break;
                case BI_CMYKRLE8: printf("BI_CMYKRLE8\n"); break;
                case BI_CMYKRLE4: printf("BI_CMYKRLE4\n"); break;
            }

            /* rows in the bitmap data are padded to be multiples of 4 bytes long */
            i32 row_size = 4 * ((color_depth * width + 31) / 32);
            i32 useful_bytes = width * color_depth / 8;
            i32 padding = row_size - useful_bytes;

            u8 *bitmap_data = buffer + offset;
            u8 *image_data = (u8 *) malloc(width * height * 4);
            u32 *int_data = (u32 *) image_data;

            for(i32 y = height - 1; y >= 0; --y)
            {
                for(i32 x = 0; x < width; ++x)
                {
                    u32 pixel = 0;
                    u8 *byte = (u8 *) &pixel;
                    /* extract rgb values*/
                    *byte++ = *bitmap_data++;
                    *byte++ = *bitmap_data++;
                    *byte++ = *bitmap_data++;
                    *byte++ = *bitmap_data++;

                    int_data[x + width * y] = pixel;
                }
                /* padding is at the end of the row */
                bitmap_data += padding;
            }
            out_image->data = image_data;
            out_image->width = width;
            out_image->height = height;
        }
        free(buffer);
        return 1;
    }
    else
    {
        fprintf(stderr, "IO error in file: %s\n\t%s\n", filename, strerror(errno));
        exit(1);
    }
    return 0;
}

void draw_player(State state, int x, int y)
{
    draw_image(state.window_buffer, guy_img, state.x_offset + x, state.y_offset + y, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
}

void draw_level(State state, Board b)
{
    fill_rect(state.window_buffer, 0, 0, state.window_width, state.window_height, 0);

    for(uint32_t x = 0; x < b.width; x++)
    {
        for(uint32_t y = 0; y < b.height; y++)
        {
            uint8_t cell_type = b.data[x + y * b.width];
            /* uint32_t color = 0; */

            /* if(is_goal(state.cell_type) && is_block(state.cell_type)) color = (255 << 16) | (255 << 8) | (0 << 0); */
            /* if(is_goal(state.cell_type) && is_start(state.cell_type)) color = (255 << 16) | (0 << 8) | (255 << 0); */
            if(is_wall(cell_type))
            {
                draw_image(state.window_buffer, wall_img, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
            }
            else if(is_block(cell_type))
            {
                draw_image(state.window_buffer, floor_img, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
                draw_image(state.window_buffer, block_img, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
            }
            else if(is_goal(cell_type))
            {
                draw_image(state.window_buffer, goal_img, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
            }
            else if(is_floor(cell_type))
            {
                draw_image(state.window_buffer, floor_img, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, 0, 0, state.cell_width, state.cell_height, state.x_scale, state.y_scale);
            }
            else if(is_outside(cell_type))
                fill_rect(state.window_buffer, state.x_offset + x * state.cell_width, state.y_offset + y * state.cell_height, state.cell_width, state.cell_height, 0);
        }
    }
    /* fill_rect(state.window_buffer, state.x_offset + player_x * state.cell_width, state.y_offset + player_y * state.cell_height, state.cell_width, state.cell_height, 255); */
}

int main(int argc, char **argv)
{
    State state = {0};
    window_width = 800;
    window_height = 800;

    // It's Xlib time!
    Display *display = XOpenDisplay(NULL);
    unsigned int screen = DefaultScreen(display);
    Window window;
    GC gc;
    XImage *window_buffer;
    XSetWindowAttributes window_attributes = {
        /* .override_redirect = 1, */
        .background_pixel = ~0,
        .border_pixel = (255 << 8) | (255 << 16),
        .event_mask = StructureNotifyMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | EnterWindowMask | LeaveWindowMask,
    };

    unsigned int display_width = DisplayWidth(display, screen);
    unsigned int display_height = DisplayHeight(display, screen);

    window = XCreateWindow(display, RootWindow(display, screen),
                           0, 0,
                           window_width, window_height,
                           2, DefaultDepth(display, screen),
                           InputOutput, DefaultVisual(display, screen),
                           CWBackPixel | CWBorderPixel | CWOverrideRedirect | CWEventMask,
                           &window_attributes);
    XMapWindow(display, window);

    gc = XCreateGC(display, window, 0, NULL);

    window_buffer = XCreateImage(display, DefaultVisual(display, screen),
                                 24, ZPixmap, 0,
                                 (char *) malloc(display_width * display_height * sizeof(uint32_t)),
                                 display_width, display_height, 32, display_width * sizeof(uint32_t));
    /* fill_rect(window_buffer, 0, 0, window_width, window_height, 0); */

    block_img = XCreateImage(display, DefaultVisual(display, screen),
                             32, ZPixmap, 0,
                             (char *) malloc(128 * 128 * sizeof(u32)),
                             128, 128, 32, 128 * sizeof(u32));

    wall_img = XCreateImage(display, DefaultVisual(display, screen),
                            32, ZPixmap, 0,
                            (char *) malloc(128 * 128 * sizeof(u32)),
                            128, 128, 32, 128 * sizeof(u32));

    floor_img = XCreateImage(display, DefaultVisual(display, screen),
                             32, ZPixmap, 0,
                             (char *) malloc(128 * 128 * sizeof(u32)),
                             128, 128, 32, 128 * sizeof(u32));

    guy_img = XCreateImage(display, DefaultVisual(display, screen),
                           32, ZPixmap, 0,
                           (char *) malloc(128 * 128 * sizeof(u32)),
                           128, 128, 32, 128 * sizeof(u32));

    goal_img = XCreateImage(display, DefaultVisual(display, screen),
                            32, ZPixmap, 0,
                            (char *) malloc(128 * 128 * sizeof(u32)),
                            128, 128, 32, 128 * sizeof(u32));

    if(!load_bmp("../data/rock.bmp", block_img)) panic("I dunno :P");
    if(!load_bmp("../data/wall.bmp", wall_img)) panic("I dunno :P");
    if(!load_bmp("../data/floor.bmp", floor_img)) panic("I dunno :P");
    if(!load_bmp("../data/guy.bmp", guy_img)) panic("I dunno :P");
    if(!load_bmp("../data/goal.bmp", goal_img)) panic("I dunno :P");

    uint32_t b_idx = 0;
    char *board_file = "../data/Microban.txt";
    for(int i = 1; i < argc; i++)
    {
        if(strncmp(argv[i], "-level", strlen("-level")) == 0)
        {
            if(i + 1 >= argc)
           {
                fprintf(stderr, "Expected a number argument after -level.\n");
                exit(1);
            }
            i++;
            int level_number = atoi(argv[i]) - 1;
            if(level_number >= 0) b_idx = level_number;
        }
        else if(strncmp(argv[i], "-file", strlen("-file")) == 0)
        {
            if(i + 1 >= argc)
            {
                fprintf(stderr, "Expected a file name argument after -file.\n");
                exit(1);
            }
            i++;
            board_file = argv[i];
        }
    }
    levels = new_arena(65536);
    BoardArray boards = parse_boards(board_file);
    if(b_idx >= boards.length) panic("There aren't %u levels you drongo!! There are %u levels\n", b_idx + 1, boards.length + 1);

    Board b = boards.data[b_idx];
    size_t stack_pointer = levels.stack_pointer;
    uint8_t *initial_b = allocate(&levels, b.width * b.height);
    memcpy(initial_b, b.data, b.width * b.height);

    uint32_t player_x = 0, player_y = 0;
    for(int i = 0; i < b.width * b.height; i++)
    {
        if(is_start(b.data[i]))
        {
            player_x = i % b.width;
            player_y = i / b.width;
            break;
        }
    }

    int cell_width = 128;
    int cell_height = 128;
    int board_pixel_width = b.width * cell_width;
    int board_pixel_height = b.height * cell_height;
    int x_offset = (window_width - board_pixel_width) / 2;
    int y_offset = (window_height - board_pixel_height) / 2;

    state.display = display;
    state.window = window;
    state.gc = gc;
    state.window_buffer = window_buffer;
    state.window_width = window_width;
    state.window_height = window_height;
    state.x_offset = x_offset;
    state.y_offset = y_offset;
    state.cell_width = cell_width;
    state.cell_height = cell_height;
    state.x_scale = 1.0;
    state.y_scale = 1.0;

    XEvent event;
    for(;;)
    {
        XNextEvent(display, &event);

        switch(event.type)
        {
            /* case EnterNotify: XGrabKeyboard(display, window, 1, GrabModeAsync, GrabModeAsync, CurrentTime); break; */

            /* case LeaveNotify: XUngrabKeyboard(display, CurrentTime); break; */

            case ConfigureNotify:
            {
                /* printf("ConfigureNotify\n"); */
                state.window_width = event.xconfigure.width;
                state.window_height = event.xconfigure.height;
            } break;

            case KeyPress:
            {
                KeySym symbol = XLookupKeysym(&event.xkey, 0);
                switch(symbol)
                {
                    case XK_Right:
                    case XK_f:
                    {
                        move_right(state, b, &player_x, &player_y);
                    } break;

                    case XK_Left:
                    case XK_s:
                    {
                        move_left(state, b, &player_x, &player_y);
                    } break;

                    case XK_Up:
                    case XK_e:
                    {
                        move_up(state, b, &player_x, &player_y);
                    } break;

                    case XK_Down:
                    case XK_d:
                    {
                        move_down(state, b, &player_x, &player_y);
                    } break;

                    case XK_r:
                    {
                        memcpy(b.data, initial_b, b.width * b.height);
                        for(int i = 0; i < b.width * b.height; i++)
                        {
                            if(is_start(b.data[i]))
                            {
                                player_x = i % b.width;
                                player_y = i / b.width;
                                break;
                            }
                        }
                    } break;

                    case XK_c:
                    {
                        int horizontal_cells = (state.window_width - 160) / b.width;
                        int vertical_cells = (state.window_height - 160) / b.height;
                        state.cell_height = state.cell_width = min(horizontal_cells, vertical_cells);
                        state.x_scale = (double) state.cell_width / 128.0;
                        state.y_scale = (double) state.cell_height / 128.0;
                    } break;

                    case XK_u:
                    {
                        if(moves_length > 0)
                        {
                            uint8_t move = moves[--moves_length];
                            if(!is_player(move))
                            {
                                if(is_left(move))
                                {
                                    b.data[player_x - 1 + player_y * b.width] &= ~BLOCK;
                                    b.data[player_x + player_y * b.width] |= BLOCK;
                                    player_x++;
                                }
                                else if(is_right(move))
                                {
                                    b.data[player_x + 1 + player_y * b.width] &= ~BLOCK;
                                    b.data[player_x + player_y * b.width] |= BLOCK;
                                    player_x--;
                                }
                                else if(is_down(move))
                                {
                                    b.data[player_x + (player_y + 1) * b.width] &= ~BLOCK;
                                    b.data[player_x + player_y * b.width] |= BLOCK;
                                    player_y--;
                                }
                                else if(is_up(move))
                                {
                                    b.data[player_x + (player_y - 1) * b.width] &= ~BLOCK;
                                    b.data[player_x + player_y * b.width] |= BLOCK;
                                    player_y++;
                                }
                            }
                            else
                            {
                                if(is_left(move)) player_x++;
                                else if(is_right(move)) player_x--;
                                else if(is_down(move)) player_y--;
                                else if(is_up(move)) player_y++;
                            }
                        }
                    } break;

                    case XK_equal:
                    {
                        state.cell_width += 16;
                        state.cell_height += 16;
                        state.x_scale = (double) state.cell_width / 128.0;
                        state.y_scale = (double) state.cell_height / 128.0;
                    } break;

                    case XK_minus:
                    {
                        if(state.cell_width >= 48 && state.cell_height >= 48)
                        {
                            state.cell_width -= 16;
                            state.cell_height -= 16;
                            state.x_scale = (double) state.cell_width / 128.0;
                            state.y_scale = (double) state.cell_height / 128.0;
                        }
                    } break;

                    case XK_q:
                    {
                        /* XUngrabKeyboard(display, CurrentTime); */
                        printf("Level = %u\nLevels memory = %lu\n", b_idx + 1, levels.stack_pointer);
                        exit(0);
                    } break;
                }
            } break;

            case ButtonPress:
            {
                // Will probably do something with the mouse later
            } break;

            case ButtonRelease:
            {
                // Will probably do something with the mouse later
            } break;
        }

        int win = 1;
        for(int i = 0; i < b.width * b.height; i++)
        {
            if(is_goal(b.data[i]) && !is_block(b.data[i]))
            {
                win = 0;
                break;
            }
        }

        if(win)
        {
            b = boards.data[++b_idx % boards.length];
            moves_length = 0;
            set_stack_pointer(&levels, stack_pointer);
            initial_b = allocate(&levels, b.width * b.height);
            memcpy(initial_b, b.data, b.width * b.height);
            for(int i = 0; i < b.width * b.height; i++)
            {
                if(is_start(b.data[i]))
                {
                    player_x = i % b.width;
                    player_y = i / b.width;
                    break;
                }
            }
        }
        board_pixel_width = b.width * state.cell_width;
        board_pixel_height = b.height * state.cell_height;
        state.x_offset = (state.window_width - board_pixel_width) / 2;
        state.y_offset = (state.window_height - board_pixel_height) / 2;
        if(state.y_offset < 0) printf("%d - %d = %d\n", state.window_height, board_pixel_height, state.y_offset << 1);
        draw_level(state, b);
        draw_player(state, player_x * state.cell_width, player_y * state.cell_height);
        XPutImage(state.display, state.window, state.gc, state.window_buffer, 0, 0, 0, 0, state.window_width, state.window_height);
    }
}
